-module(ferbot_sensors).

-behavior(gen_server).

-export([init/1, handle_call/3, handle_cast/2,
         handle_info/2, code_change/3, terminate/2]).
-export([start_link/0, read_proximity/1, read_ambient/1]).

-define(LEFT,   1).
-define(RIGHT,  0).
-define(CENTER, 2).
-define(BUS,    "i2c-1").
-define(SERVER, ?MODULE).

-record(sensors, {mux, prox_left, prox_right, prox_center}).
-record(fs_state, {sensors, readings}).

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%% gen_server callbacks

init([]) ->
    Sensors = start_sensors(),
    {ok, #fs_state{ sensors=Sensors, readings=not_ready}}.

handle_call(get_readings, _From, State) ->
    {reply, State#fs_state.readings, State#fs_state{readings = not_ready}}.

handle_cast(_Request, State) ->
    {ok, State}.

handle_info({readings, Ambient, Proximity}, State) ->
    {ok, State#fs_state{readings = {Ambient, Proximity}}}.

code_change(_, _, State) ->
    {ok, State}.

terminate(shutdown, #fs_state{sensors=Sensors}) ->
    stop_sensors(Sensors),
    ok.

%%% Sensor code

stop_sensors(Sensors=#sensors{mux=Mux}) ->
    tca9548a:select(Mux, ?LEFT),
    vcnl4010:stop(Sensors#sensors.prox_left),
    tca9548a:select(Mux, ?RIGHT),
    vcnl4010:stop(Sensors#sensors.prox_right),
    tca9548a:select(Mux, ?CENTER),
    vcnl4010:stop(Sensors#sensors.prox_center),
    tca9548a:stop(Mux).

start_sensors() ->
    {ok, Mux} = tca9548a:start_link(?BUS),
    {ok, ProxLeft}   = start_prox(?LEFT,   Mux),
    vcnl4010:set_proximity_rate(ProxLeft, 31.25),
    vcnl4010:set_ambient_rate(ProxLeft, 8#7),
    vcnl4010:mode(ProxLeft, periodic),

    {ok, ProxRight}  = start_prox(?RIGHT,  Mux),
    vcnl4010:set_proximity_rate(ProxRight, 31.25),
    vcnl4010:set_ambient_rate(ProxRight, 8#7),
    vcnl4010:mode(ProxRight, periodic),

    {ok, ProxCenter} = start_prox(?CENTER, Mux),
    vcnl4010:set_proximity_rate(ProxCenter, 31.25),
    vcnl4010:set_ambient_rate(ProxCenter, 8#7),
    vcnl4010:mode(ProxCenter, periodic),

    #sensors{mux         = Mux,
             prox_left   = ProxLeft,
             prox_right  = ProxRight,
             prox_center = ProxCenter}.

start_prox(N, Mux) ->
    tca9548a:select(Mux, N),
    vcnl4010:start_link(?BUS).

read_proximity(Sensors=#sensors{mux=Mux}) ->
    tca9548a:select(Mux, ?LEFT),
    LeftProx = vcnl4010:read_proximity(Sensors#sensors.prox_left),

    tca9548a:select(Mux, ?RIGHT),
    RightProx = vcnl4010:read_proximity(Sensors#sensors.prox_right),

    tca9548a:select(Mux, ?CENTER),
    CenterProx = vcnl4010:read_proximity(Sensors#sensors.prox_center),

    [LeftProx, RightProx, CenterProx].

read_ambient(Sensors=#sensors{mux=Mux}) ->
    tca9548a:select(Mux, ?LEFT),
    LeftAmbient = vcnl4010:read_ambient(Sensors#sensors.prox_left),

    tca9548a:select(Mux, ?RIGHT),
    RightAmbient = vcnl4010:read_ambient(Sensors#sensors.prox_right),

    tca9548a:select(Mux, ?CENTER),
    CenterAmbient = vcnl4010:read_ambient(Sensors#sensors.prox_center),

    [LeftAmbient, RightAmbient, CenterAmbient].
