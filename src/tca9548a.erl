-module(tca9548a).

-export([start_link/1, start_link/2, stop/1, select/2]).

-define(TCA9548A_ADDR, 16#70).

start_link(Bus) ->
    i2c:start_link(Bus, ?TCA9548A_ADDR).

start_link(Bus, Address) ->
    i2c:start_link(Bus, Address).

stop(Device) ->
    i2c:stop(Device).

select(_, Channel) when (Channel < 0) or (Channel > 8) ->
    erlang:error(badarg);
select(Device, Channel) ->
    i2c:write(Device, <<(1 bsl Channel):8>>).
